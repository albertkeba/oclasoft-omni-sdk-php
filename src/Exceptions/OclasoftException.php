<?php

namespace Omni\Exceptions;

class OmniException extends \Exception
{
    protected $message = 'Bad request';
    protected $code = 400;

    public function __construct($message = '', $code = null) {
        if ($message != '')
            $this->message = $message;

        if ($code != null)
            $this->code = $code;

        parent::__construct($this->message, $this->code, null);
    }
}
