<?php

namespace Omni\Exceptions;

class InsufficientCreditException extends OmniException
{
    protected $message = 'Forbidden, Insufficient credit';
    protected $code = 403;

    public function __construct($message = '') {
        if ($message != '')
            $this->message = $message;

        parent::__construct($this->message, $this->code, null);
    }
}
