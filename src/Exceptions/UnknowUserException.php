<?php

namespace Omni\Exceptions;

class UnknowUserException extends OmniException
{
    protected $message = 'Unknow user';
    protected $code = 401;

    public function __construct($message = '') {
        if ($message != '')
            $this->message = $message;

        parent::__construct($this->message, $this->code, null);
    }
}
