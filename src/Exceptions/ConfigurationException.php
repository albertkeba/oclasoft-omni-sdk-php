<?php

namespace Omni\Exceptions;

class ConfigurationException extends OmniException
{
    protected $message = 'Credentials are required to create a Client';
    protected $code = 400;

    public function __construct($message = '', $code = null) {
        if ($message != '')
            $this->message = $message;

        if ($code != null)
            $this->code = $code;

        parent::__construct($this->message, $this->code, null);
    }
}
