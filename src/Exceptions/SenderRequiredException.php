<?php

namespace Omni\Exceptions;

class SenderRequiredException extends OmniException
{
    protected $message = 'Sender is required';
    protected $code = 422;

    public function __construct($message = '') {
        if ($message != '')
            $this->message = $message;

        parent::__construct($this->message, $this->code, null);
    }
}
