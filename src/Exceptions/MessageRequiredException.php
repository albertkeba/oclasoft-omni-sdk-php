<?php

namespace Omni\Exceptions;

class MessageRequiredException extends OmniException
{
    protected $message = 'Message is required';
    protected $code = 422;

    public function __construct($message = '') {
        if ($message != '')
            $this->message = $message;

        parent::__construct($this->message, $this->code, null);
    }
}
