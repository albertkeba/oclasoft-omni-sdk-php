<?php

namespace Omni\Exceptions;

class NumberRequiredException extends OmniException
{
    protected $message = 'A phone number is required';
    protected $code = 422;

    public function __construct($message = '') {
        if ($message != '')
            $this->message = $message;

        parent::__construct($this->message, $this->code, null);
    }
}
