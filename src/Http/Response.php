<?php

namespace Omni\Http;

class Response
{
    protected $headers;
    protected $content;
    protected $statusCode;

    /**
     * [__construct description]
     * @param integer $statusCode [description]
     * @param string $content    [description]
     * @param array  $headers    [description]
     */
    public function __construct($statusCode, $content, $headers = array()) {
        $this->statusCode = $statusCode;
        $this->content = $content;
        $this->headers = $headers;
	}

    /**
     * [getContent description]
     * @return string
     */
    public function getContent() {
        return json_decode($this->content, true);
    }

    /**
     * [getStatusCode description]
     * @return string
     */
    public function getStatusCode() {
        return $this->statusCode;
    }

    /**
     * [getHeaders description]
     * @return string
     */
    public function getHeaders() {
        return $this->headers;
    }

    /**
     * [ok description]
     * @return integer
     */
    public function ok() {
        return $this->getStatusCode() < 400;
    }

    /**
     * [__toString description]
     * @return string [description]
     */
    public function __toString() {
        return '[Response] HTTP ' . $this->getStatusCode() . ' ' . $this->content;
    }
}
