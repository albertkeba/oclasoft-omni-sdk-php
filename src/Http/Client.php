<?php

namespace Omni\Http;

interface Client
{
    /**
     * [request description]
     * @param  [type] $method [description]
     * @param  array  $params [description]
     * @return [type]         [description]
     */
     public function request($method, $params = array(), $uri = '', $headers = array(), $username = null, $password = null, $timeout = null);
}
