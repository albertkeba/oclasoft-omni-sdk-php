<?php

namespace Omni\Http;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\ClientInterface;
use Omni\Exceptions\HttpException;
use GuzzleHttp\Exception\BadResponseException;

class GuzzleClient implements Client
{
	private $client;

	public function __construct(ClientInterface $client) {
		$this->client = $client;
	}

	public function request($method, $params = [], $url = '', $data = [], $headers = [], $user = null, $password = null, $timeout = null) {
        try {
            $request = new Request($method, $url, $headers);

            $response = $this->client->send($request, array(
                'timeout' => $timeout,
                'auth' => array($user, $password),
                'query' => $params,
                'form_params' => $data
            ));
        } catch (BadResponseException $exception) {
            $response = $exception->getResponse();
        } catch (\Exception $exception) {
            throw new HttpException('Unable to complete the HTTP request', 0, $exception);
        }

        return new Response($response->getStatusCode(), (string) $response->getBody(), $response->getHeaders());
    }
}
