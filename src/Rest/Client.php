<?php

namespace Omni\Rest;

use GuzzleHttp\Client as GuzzleClientInterface;
use Omni\Http\GuzzleClient;
use Omni\Exceptions\OmniException;
use Omni\Exceptions\ConfigurationException;
use Omni\Exceptions\SenderRequiredException;
use Omni\Exceptions\NumberRequiredException;
use Omni\Exceptions\MessageRequiredException;
use Omni\Exceptions\UnknowUserException;
use Omni\Exceptions\InsufficientCreditException;

/**
 * A client for accessing the Omni OMNI API
 *
 * @author Albert KEBA MAGNAGNA <albert.keba@gmail.com>
 */
class Client
{
    /** @var string $username contains user's name for the Client*/
    protected $username;

    /** @var string $password contains user's password for the Client*/
    protected $password;

    /** @var string $apiBaseUrl contains uri string for the Client*/
    protected $apiBaseUrl;

    /** @var Omni\Http\GuzzleClient $username contains curl client for the Client*/
    protected $httpClient;

    /**
     * Initialize the Omni OMNI Client
     *
     * Here's an example of creating a client
     *
     * $client = new Omni\Rest\Client('username', 'password', 'http://www.Omni-foo/');
     *
     * @param string $username   Omni user name
     * @param string $password   Omni user password
     * @param string $apiBaseUrl Omni URI string
     * @throws Omni\Exceptions\ConfigurationException if the provided argument is missing
     */
    public function __construct($username, $password, $apiBaseUrl) {
        if ($username == null) throw new ConfigurationException('Username is required to create a Client', 401);
        if ($password == null) throw new ConfigurationException('Password is required to create a Client', 401);
        if ($apiBaseUrl == null) throw new ConfigurationException('API uri is required to create a Client', 422);

        $this->username = $username;
        $this->password = $password;
        $this->apiBaseUrl = $apiBaseUrl;

        $this->httpClient = new GuzzleClient(new GuzzleClientInterface(array(
            'base_uri' => $apiBaseUrl
        )));
    }

    /**
     * send SMS to provided number
     *
     * @param  string $sender  sender name
     * @param  string $numbers receiver(s) phone number
     * @param  string $message text message content
     * @param  string $method  http method
     * @throws Omni\Exceptions\SenderRequiredException if $sender argument is missing
     * @throws Omni\Exceptions\NumberRequiredException if $numbers argument is missing
     * @throws Omni\Exceptions\MessageRequiredException if $message argument is missing
     * @throws Omni\Exceptions\UnknowUserException if a use is not recognized
     * @throws Omni\Exceptions\InsufficientCreditException if account's credit is insufficient
     *
     * @return Omni\Http\Response
     */
    public function send($sender, $numbers, $message, $method = 'GET') {
        if ($sender == null) throw new SenderRequiredException();
        if ($numbers == null) throw new NumberRequiredException();
        if ($message == null) throw new MessageRequiredException();

        $params = array(
            'username' => $this->username,
            'password' => $this->password,
            'sender' => $sender,
            'mobile' => $numbers,
            'text' => $message
        );

        $uri = 'sms/';

        $response = $this->httpClient->request($method, $params, $uri);
        $content = $response->getContent();

        if ($response->getStatusCode() != 200) {
            throw new OmniException();
        }

        if (isset($content['error'])) {
            switch ($content['error']) {
                case '-2005': throw new UnknowUserException();
                    break;
                case '-2007': throw new InsufficientCreditException();
                    break;
            }
        }

        return $response;
    }

    /**
     * get credit account
     *
     * @return int
     */
    public function getBalance() {
        $params = array(
            'username' => $this->username,
            'password' => $this->password,
        );

        $uri = 'credit/get/';

        $response = $this->httpClient->request('GET', $params, $uri);
        $content = $response->getContent();

        return $content['credit'];
    }

    /**
     * send an Http request
     *
     * @param  string $method [description]
     * @param  string $uri    [description]
     * @param  array  $params [description]
     *
     * @return Omni\Http\Response
     */
     public function request($method, $params = array(), $uri = '', $headers = array(), $username = null, $password = null, $timeout = null) {
        $username = $username ? $username : $this->username;
        $password = $password ? $password : $this->password;

        $headers['Accept-Charset'] = 'utf-8';

        if (! array_key_exists('Accept', $headers)) {
            $headers['Accept'] = 'application/json';
        }

        return $this
                    ->getHttpClient()
                    ->request($method, $uri, $params, $data, $headers, $username, $password, $timeout);
	}

    /**
     * get HttpClient
     *
     * @return Omni\Http\GuzzleClient
     */
    public function getHttpClient() {
        return $this->httpClient;
    }

    /**
     * set HttpClient
     *
     * @param Omni\Http\GuzzleClient $httpClient [description]
     */
    public function setHttpClient(GuzzleClient $httpClient) {
        $this->httpClient = $httpClient;
    }

    /**
     * Return username
     *
     * @return string
     */
    public function getUsername() {
        return $this->username;
    }

    /**
     * Set username
     * @param string $username
     */
    public function setUsername($username) {
    	$this->username = $username;
    }

    /**
     * Return password
     *
     * @return string
     */
    public function getPassword() {
    	return $this->password;
    }

    /**
     * set password
     *
     * @param string $password
     */
    public function setPassword($password) {
    	$this->password = $password;
    }
}
