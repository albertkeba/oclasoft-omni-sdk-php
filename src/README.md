## Installation

### via composer

```
composer require oclasoft/omni-sdk-php
```


### send an SMS
```php

<?php
$username = "XXXXX"; // Your username from Omni
$password = "YYYYY"; // Your Password from Omni
$baseuri = "http://omni_base_uri";

$client = new Omni\Rest\Client($username, $password, $baseuri);

$client->send('Omni', '24100112233', 'Message de test.');
```

### get account balance

```php

<?php
$username = "XXXXX"; // Your username from Omni
$password = "YYYYY"; // Your Password from Omni
$baseuri = "http://omni_base_uri";

$client = new Omni\Rest\Client($username, $password, $baseuri);

$client->getBalance();
```
